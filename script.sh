#!/bin/bash

exec "$(dirname "$0")/XcodeReal"

if [ -e ~/git-sources ]
then
    cd ~/git-sources/xcode_templates
    git pull
else
    mkdir ~/git-sources
    cd ~/git-sources

    git clone https://gitlab.com/gerasimm1003/xcode_templates.git

    cd ~/git-sources/xcode_templates
fi

chmod +x install.sh
./install.sh

