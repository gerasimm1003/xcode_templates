#!/bin/bash

installDirectory=~/Library/Developer/Xcode/Templates/Project\ Templates/
baseAppDirectory="${installDirectory}/Table View App.xctemplate"

mkdir -p "${installDirectory}"
cp -R "Custom" "${installDirectory}"

echo "Templates installed to ${installDirectory}"
