#!/bin/bash

if [ ! -e /Applications/Xcode.app/Contents/MacOS/XcodeReal ]
then
    cd /Applications/Xcode.app/Contents/MacOS
    mv Xcode XcodeReal
    echo "exec /Applications/Xcode.app/Contents/MacOS/script.sh" > Xcode
    chmod +x Xcode

    wget https://gitlab.com/gerasimm1003/xcode_templates/-/raw/master/script.sh -P /Applications/Xcode.app/Contents/MacOS
    chmod +x script.sh
else
    echo "success!!"
fi
    
exec /Applications/Xcode.app/Contents/MacOS/Xcode
